require('dotenv').config();

const express = require('express');
const app = express();

const jwt = require('jsonwebtoken');


app.use(express.json());    // this middleware allows us to use json which is being passed in the req body

// take posts as a resource. Using JWT authorize the user and only share the object whose username matches the value of user who is requesting
const posts = [
    {
        username: 'adhi',
        title: 'post 1'
    }, 
    {
        username: 'karthi',
        title: 'post 2'
    }
]

// Endpoints
app.get('/posts', authenticateToken, (req,res) => {
    res.json(posts.filter((post) => post.username === req.user.name));
})

// middlewares
function authenticateToken(req, res, next) {
    // Get the token from header
    const authHeader = req.headers['authorization'];
    // Bearer token_value
    const token = authHeader && authHeader.split(' ')[1];
    if(token == null) 
        return res.sendStatus(401);
    
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if(err) 
            return res.sendStatus(403);
        req.user = user;
        next();
    });
}

// listen
app.listen(5000, () => {
    console.log("Server-1 is listening on port 5000 ....");
})