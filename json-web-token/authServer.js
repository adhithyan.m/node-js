require('dotenv').config();

const express = require('express');
const app = express();

const jwt = require('jsonwebtoken');

let refreshTokens = []; // In real world this should be in DB or cache system like redis

app.use(express.json());    // this middleware allows us to use json which is being passed in the req body

// Endpoints
app.post('/token', (req,res) => {
    const refreshToken = req.body.token;
    if(refreshToken == null) 
        return res.sendStatus(401);
    if(!refreshTokens.includes(refreshToken))
        return res.sendStatus(403);
    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
        if(err)
            return res.sendStatus(403);
        const accessToken = generateAccessToken({ name: user.name });
        res.json({ accessToken: accessToken});
    });
})
app.delete('/logout', (req,res) => {
    refreshTokens = refreshTokens.filter((token) => token !== req.body.token);
    res.send(204);
})
app.post('/login', (req,res) => {
    // Authenticate User
    const username = req.body.username;
    const user = { name: username };

    const accessToken = generateAccessToken(user);
    const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET);
    refreshTokens.push(refreshToken);
    res.json({ accessToken: accessToken, refreshToken: refreshToken });
})

// Util functions
function generateAccessToken(user) {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '15s' })
}

// listen
app.listen(6000, () => {
    console.log("Server-2 is listening on port 6000 ....");
})