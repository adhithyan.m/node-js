// var name = 'adhi';
const name = 'adhi';
let age = 29;
const hasHobbies = true;

// name = 'karthi'  // const
age = 30;   // let is modifiable

const summarizeUser = (userName, userAge, userHasHobby) => {
    return (
        'Name is '+userName
        +
        ', Age is '+userAge
        +
        ' and the user has hobbies: '+userHasHobby
    );
};

const add = (a,b) => {
    return a+b;
}

const add2 = (a,b) => a+b;

const add3 = a => a+1;

const add4 = () => 1+2;

console.log(add(1,2));
console.log(add2(3,5));
console.log(add3(3));
console.log(add4());
console.log(summarizeUser(name,age,hasHobbies));