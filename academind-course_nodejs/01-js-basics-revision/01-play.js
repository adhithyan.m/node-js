// var name = 'adhi';
const name = 'adhi';
let age = 29;
const hasHobbies = true;

// name = 'karthi'  // const
age = 30;   // let is modifiable

function summarizeUser(userName, userAge, userHasHobby) {
    return (
        'Name is '+userName
        +
        ', Age is '+userAge
        +
        ' and the user has hobbies: '+userHasHobby
    );
}

console.log(summarizeUser(name,age,hasHobbies));