const hobbies = ['sports', 'çooking', 1, true, { name: 'adhi'}, ['another arr']];

for(let hobby of hobbies) {
    console.log(hobby);
}

console.log(hobbies.map(
    hobby => 'Hobby: '+hobby
)); // transforms the array and returns a new one
console.log('hobbies: '+ hobbies);

const coppiedArray = hobbies.slice();
console.log(coppiedArray);
const coppiedArray2 = [...hobbies];     // spread operator
console.log(coppiedArray2);

const toArray = (arg1, arg2, arg3) => {
    return [arg1, arg2, arg3];
}
console.log(toArray(1,2,3));

const toArray2 = (...args) => {
    return args;
}
console.log(toArray2(1,23,4,5,232));