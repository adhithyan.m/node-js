const person = {
    name: 'adhi',
    age: 29, 
    // greet: () => {
    //     console.log('Hi, I am '+this.name);  //undefined due to person being global scope
    // }
    greet() {
        console.log('Hi, I am '+this.name); // 'adhi'
    }
};

person.greet();

const printName = ({ name, age }) => {
    return `name: ${name}, age: ${age}`;
}

const { name, age } = person;
console.log(name, age, printName(person));