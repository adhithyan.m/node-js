const path = require('path');

// we are gonna export a variable which helps us construct the path to the parent directory
module.exports = path.dirname(process.mainModule.filename);

// dirName - returns the directory name of the path.
// process - global process variable which is available in all files
// mainModule - property available in process variable ( this is the main module which started our application)