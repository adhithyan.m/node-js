const express = require('express');
const path = require('path');
const rootDir = require('../util/path');
const adminData = require('./admin');

const router = express.Router();

router.use('/', (req,res,next) => {
    console.log('This always runs!');
    next(); // Allows the request to continue to the next middleware in line.
});
router.use('/', (req,res,next) => {
    console.log('Inside another middleware.');
    //res.status(200).end('<h1>Hello World</h1>');
    next();
});
router.get('/', (req,res) => {
    // res.status(200).send('<h1>Hello world</h1>');
    // res.sendFile(path.join(__dirname, '../', 'views', 'shop.html'));
    // console.log('shop.js: ',adminData.products);
    // res.sendFile(path.join(rootDir, 'views', 'shop.html'));
    const products = adminData.products;
    res.render('shop', {prods: products, pageTitle: 'My Shop', path: '/'});   // renders the shop.pug file
});

module.exports = router;