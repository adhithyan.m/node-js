const express = require('express');
const path = require('path');

const rootDir = require('../util/path');

const router = express.Router();

const products = [];

router.get('/add-product', (req,res) => {
    // res.send(`<h1>The "Add product" Page</h1>`);
    // res.send('<form action="/admin/product" method="POST"><input type="text" name="title"><button type="submit">Add Product</button></form>');
    // res.sendFile(path.join(__dirname, '../' 'views', 'add-product.html'));
    // res.sendFile(path.join(rootDir, 'views', 'add-product.html'));
    res.render('add-product', { pageTitle: 'Add Product', path: '/admin/add-product' });
});
router.post('/product', (req,res) => {
    // console.log("request body: ", req.body);
    products.push({ title: req.body.title });
    res.redirect('/');  
});

exports.routes = router;
exports.products = products;