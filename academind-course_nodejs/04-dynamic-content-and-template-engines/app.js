const bodyParser = require('body-parser');
const express = require('express');
const path = require('path');

const app = express();

// configuring what template engine we use for views and where the views is located
app.set('view engine', 'pug');
app.set('views', 'views');

const adminData = require('./routes/admin');
const shopRoutes = require('./routes/shop');

app.use(bodyParser.urlencoded());  // body parser
app.use(express.static(path.join(__dirname, 'public')));   // server files statically from public folder

app.use('/admin',adminData.routes);
app.use(shopRoutes);

// catch all route
app.use((req,res,next) => {
    // res.status(404).send('<h1>Page Not Found</h1>');
    // res.status(404).sendFile(path.join(__dirname, './', 'views', '404.html'));
    res.render('404', { pageTitle: 'Page Not Found' });
});

app.listen(5000, () => {
    console.log('Server is listening on port 5000...');
})