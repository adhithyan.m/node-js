const fs = require('fs');

const requestHandler = (req,res) => {
    const url = req.url;
    const method = req.method;
    
    if(url === '/') {
        res.write('<html>');
        res.write('<head><title>Enter message</title></head>');
        res.write('<body><form action="/message" method="POST"><input type="text" name="message"/><button type="submit">submit</button></form></body>');
        res.write('</html>');
        return res.end(); //response is sent to client. can't do anything after this.
    }
    if(url === '/message' && method === 'POST') {
        const body = [];
        req.on('data', (chunck) => {
            console.log(chunck);
            body.push(chunck);
        });
        return req.on('end', () => {
            const parsedBody = Buffer.concat(body).toString();
            const message = parsedBody.split('=')[1];
            fs.writeFile('message.txt', message, err => {
                res.statusCode = 302; //redirection
                res.setHeader('Location', '/');
                return res.end();
            });
        });
    }
    //console.log(req);
    // process.exit();  // exits the server. hard exit the event loop
    res.setHeader('Content-Type','text/html');
    res.write('<html>');
    res.write('<head><title>My First Page</title></head>');
    res.write('<body><h1>Hello from my node.js server!</h1></body>');
    res.write('</html>');
    res.end(); //response is sent to client. can't do anything after this.
};

module.exports = requestHandler;

// module.exports = {
//     handler : requestHandler,
//     someText : 'hello world'
// };

// module.exports.handler = requestHandler;

// exports.handler = requestHandler;