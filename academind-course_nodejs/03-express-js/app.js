const bodyParser = require('body-parser');
const express = require('express');
const path = require('path');

const app = express();

const adminRoutes = require('./routes/admin.js');
const shopRoutes = require('./routes/shop.js');

app.use(bodyParser.urlencoded());  // body parser
app.use(express.static(path.join(__dirname, 'public')));   // server files statically from public folder

app.use('/admin',adminRoutes);
app.use(shopRoutes);

// catch all route
app.use((req,res,next) => {
    // res.status(404).send('<h1>Page Not Found</h1>');
    res.status(404).sendFile(path.join(__dirname, './', 'views', '404.html'));
});

app.listen(5000, () => {
    console.log('Server is listening on port 5000...');
})