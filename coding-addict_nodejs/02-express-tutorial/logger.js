const logger = (req,res,next) => {
    const method = req.method;
    const url = req.url;
    const time = new Date().getFullYear();
    console.log({ method, url, time});
    // when you work with middleware, you must (must) need to pass it to next middleware
    // if i send res here itself then the app logic for '/' will never run
    // res.send('middleware stopped your further code and res been send');`
    next();
}

module.exports = logger;