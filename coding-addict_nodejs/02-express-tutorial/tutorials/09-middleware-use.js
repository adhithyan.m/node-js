const express = require('express');
const app = express();
const logger = require('./logger');
const authorize = require('./authorize');

// middleware - it exists between request and response
// req => middleware => res
// app.use(logger);    // applies middleware logger to all the paths
// app.use('/api', logger);       // only apply middleware to routes with path starting with /api
app.use([authorize, logger]);     // first authorize will be called then logger

app.get('/', (req,res) => {
    console.log(req.user);
    res.send('home page');
});
app.get('/about', (req,res) => {
    res.send('about page');
});
app.get('/api/products', (req,res) => {
    res.send('Products');
});
app.get('/api/items', (req,res) => {
    res.send('Items');
});
// app.get('/api/items', [authorize, logger], (req,res) => {
//     res.send('Items');
// });

app.all('*',(req,res) => {
    res.status(404).send('resource not found!');
});

app.listen(5000, () => {
    console.log('server is listening on port 5000....');
});