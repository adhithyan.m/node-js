//const app = require('express')();
const express = require('express');
const app = express();
const path = require('path');

// app.use(app.static('./public'));

// setup static and middleware
app.use(express.static('./public'));

app.get('/', (req,res) => {
    res.status(200).sendFile(path.resolve(__dirname, './navbar-app/index.html'));
});

app.all('*', (req,res) => {
    res.status(404).send('resourse not found');
})

app.listen(5000, () => {
    console.log('server is listening on port 5000 ....');
});