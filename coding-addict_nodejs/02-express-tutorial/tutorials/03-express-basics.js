const express = require('express');
// const app = require('express')();

const app = express();    // similar to http.createServer

app.get('/', (req,res) => {
    res.status(200);
    res.send('home page');
});
app.get('/about', (req,res) => {
    res.status(200).send('About page');
});

app.all('*', (req,res) => {
    res.status(404).send('<h1>resource not found</h1>');
});

app.listen(5000, () => {
    console.log('server is listening on port 5000');
});

// app.get                      fetch
// app.post                     insert
// app.put                      update
// app.delete                   delete
// app.all                      for all the above
// app.use                      for middleware
// app.listen                   which port we listen to
