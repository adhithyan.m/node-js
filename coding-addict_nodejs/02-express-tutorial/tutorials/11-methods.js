const express = require('express');
const app = express();
let { people } = require('./data');

// static assets
app.use(express.static('./methods-public'));

// parse form data
app.use(express.urlencoded({ extended: false }));   // parses form data and adds it to req body
// parse json
app.use(express.json());

app.get('/api/people', (req,res) => {
    res.status(200).json({status:true, data: people});
});
app.post('/api/people', (req,res) => {
    const {name} = req.body;
    if(!name) {
        return res.status(400).json({success: false, msg: 'please provide name value'});
    }
    res.status(201).json({success: true,person: name});
});

app.post('/api/postman/people', (req,res) => {
    const {name} = req.body;
    if(!name) {
        return res.status(400).json({success:false, msg: 'please provide name value'});
    }
    res.status(201).json({status:true,data: [...people, name]});
});
app.put('/api/people/:id', (req,res) => {
    const { id } = req.params;
    const { name } = req.body;
    // console.log(id, name);
    const person = people.find((person) => person.id === Number(id));
    if(!person) {
        return res.status(404).json({success:false, msg:`no person with id ${id}`});
    }
    const newPeople = people.map((person) => {
        if(person.id === Number(id)) {
            person.name = name;
        }
        return person;
    });
    res.status(200).json({status:true, data: newPeople});
});
app.delete('/api/people/:id', (req,res) => {
    const person = people.find((person) => person.id === Number(req.params.id));
    if(!person) {
        return res.status(404).json({status:false,msg:`no person with id ${req.params.id}`});
    }
    const newPeople = people.filter((person) => person.id !== Number(req.params.id));
    res.status(200).json({status:true, data:newPeople});
})

app.post('/login', (req,res) => {
    // console.log(req.body);
    const name = req.body.name;
    if(name) {
        return res.status(200).send(`Welcome ${name}`);
    }
    res.status(401).send('Please provide credentials');
});

app.all('*',(req,res) => {
    res.status(404).send('resource not found!');
});

app.listen(5000, () => {
    console.log('server is listening on port 5000....');
});