const express = require('express');
const app = express();
const { products } = require('./data.js');

app.get('/', (req,res) => {
    res.status(200).send('<h1>Home page</h1><a href="/api/products">products</a>');
});

app.get('/api/products', (req,res) => {
    const newProducts = products.map((product) => {
        const { id, name, image } = product;
        return { id, name, image };
    });
    res.status(200).json(newProducts);      // send a json response
});

// path params
app.get('/api/products/:productID', (req,res) => {
    console.log(req.params);
    const { productID } = req.params; 
    const singleProduct = products.find((product) => product.id === Number(productID));
    if(!singleProduct) {
        res.status(404).send(`Product doesn't exist`);
    }
    res.status(200).json(singleProduct);
})

app.get('/api/products/:productID/reviews/:reviewID', (req,res) => {
    // console.log(req.params);
    res.status(200).send('hello world this resource is under construction');
})

// query params
app.get('/api/v1/query', (req,res) => {
    // console.log(req.query);
    let { search, limit } = req.query;
    let sortedProducts = [...products];
    if(search) {
        sortedProducts = sortedProducts.filter((product) => product.name.startsWith(search));
    }
    if(limit) {
        sortedProducts = sortedProducts.slice(0, Number(limit));
    }
    if(sortedProducts.length < 1) {
        // res.status(200).send('No products matches the given criteria');
        return res.status(200).json({ status:"true", data: [] });
    }
    return res.status(200).json({ status:"true", data: sortedProducts });
})

app.all('*', (req,res) => {
    res.status(404).send('resource not found');
});

app.listen(5000, () => {
    console.log('server is listening on port 5000 ....');
})