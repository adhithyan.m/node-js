// CommonJS, every file is module (by default)
// Modules - Encapsulated Code (only share minimum)
const {john, peter} = require('./04-names.js');
// const names = require('./04-names.js');
const sayHi = require('./05-utils.js');
// console.log(names);
require('./07-mind-grenade');    // will execute the codes of this file before setting up.

sayHi("adhi");
sayHi(john);
sayHi(peter);