// local
const secretKey = "SUPER SECRET PASSWORD THAT OTHER FILES SHOULD NOT KNOW";   //well in real life we put in .env file in secured manner
// share
const john = "john";
const peter = "peter";

module.exports = {john,peter};  //this will be exported

// console.log(module);