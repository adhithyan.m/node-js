const http = require('http');

// const server = http.createServer((req,res) => {
//     console.log(req);
//     res.write('this is a server listening to port 5000');
//     res.end();
// });

const server = http.createServer((req,res) => {
    // console.log(req);
    if(req.url === '/') {
        res.end('this is a server listening to port 5000');
    }
    else if(req.url === '/about') {
        res.end('about page');
    }
    else if(req.url === '/contact') {
        res.end('contact page');
    }
    else {
        res.write(`<h1>Oops!</h1>
                <p>We can't seem to have the page you are looking for, sorry for the inconvinience</p>
                <a href="/"><btn>back home</btn></a>
                `);
        res.end();
    }
});

server.listen(5000);
