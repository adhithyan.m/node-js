const { readFile } = require('fs');

console.log('started a first task');   // 1
// CHECK FILE PATH!!
readFile('./content/first.txt','utf8', (err,result) => {
    if(err) {
        console.log(err);
        return;
    }
    console.log(result);       // 3
    console.log('completed first task');    // 4
});
console.log('starting next task');     // 2