const http = require('http');

const server = http.createServer((req,res) => {
    console.log('request event');
    res.end('hello world');
});

// server.listen is a async operation which act as event loop
server.listen(5000, () => {
    console.log('Server listening on port : 5000.......');
});