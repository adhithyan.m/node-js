const http = require('http');

// const server = http.createServer((req,res) => {
//     console.log('call received by the server');
//     res.end('hello world');
// });

// Using Event Emitter API
const server = http.createServer();
// emits request event
// subscribe to it / listen to it / respond to it
// request is a event of http.
server.on('request', (req,res) => {
    res.end('hello world');
});
server.listen(5000);